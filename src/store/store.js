import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    /* These are set to a default state when a new session has started.
    * Not sure if other variables can be nil or also need a default state.
    */
    themeColor: '#08252f',
    darkMode: 'false'
  },

  mutations: {
    setThemeColor (state, color) {
      state.themeColor = color
    },
    setDarkMode (state) {
      state.darkMode = !state.darkMode
    },

    initialiseStore(state) {
      // Check if the ID exists
      if(localStorage.getItem('store')) {
        this.replaceState(
          Object.assign(state, JSON.parse(localStorage.getItem('store')))
        )
      }
    }
  },
  getters: {
    themeColor: state => state.themeColor,
    darkMode: state => state.darkMode
  }
});

store.subscribe((mutation, state) => {
  localStorage.setItem('store', JSON.stringify(state))
});
