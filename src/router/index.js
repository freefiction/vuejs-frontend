import Router from 'vue-router'

import Home from '@/components/Home'
import Books from '@/components/books/Index'
import Book from '@/components/books/Show'
import Communities from '@/components/communities/Index'

export default new Router({
  mode: 'history',
  routes: [
    {path: '/', component: Home},
    {path: '/books', component: Books},
    {path: '/books/:id', component: Book},
    {path: '/communities', component: Communities}
  ]
})
