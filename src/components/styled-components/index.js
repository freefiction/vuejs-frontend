import Vue from 'vue'

import ThemedButton from './ThemedButton'
import ThemedPanel from './ThemedPanel'

export const themedButton = Vue.component('themed-btn', {
  components: { ThemedButton },
  template: `<ThemedButton :color="$store.getters['themeColor']"><slot></slot></ThemedButton>`,
});

export const themedPanel = Vue.component('themed-panel', {
  components: { ThemedPanel },
  template: `<ThemedPanel :color="$store.getters['themeColor']"><slot></slot></ThemedPanel>`,
});
